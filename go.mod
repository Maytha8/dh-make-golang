module github.com/Debian/dh-make-golang

go 1.16

require (
	github.com/charmbracelet/glamour v0.3.0
	github.com/google/go-github/v38 v38.1.0
	github.com/gregjones/httpcache v0.0.0-20190611155906-901d90724c79
	github.com/mattn/go-isatty v0.0.12
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/net v0.0.0-20210331212208-0fccb6fa2b5c
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	golang.org/x/tools v0.0.0-20191119224855-298f0cb1881e
	pault.ag/go/debian v0.12.0
)
